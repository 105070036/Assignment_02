# Software Studio 2019 Spring Assignment 2

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

玩家控制飛機攻擊敵人，攻擊boss，途中獲得道具後可以根據指示釋放技能，並且關卡一關比一關困難

玩家在menu進入遊戲後，從level 1開始，攻擊敵人獲得分數，累積在左上角，生命數顯示在右上角。

前進一定程度之後，

道具會隨機出現在地圖上，'準心'圖樣的道具
• [自動瞄準敵人的模式，透過ctrlBotton控制模式的開關]
• [攻擊變快速、變強，透過ctrlBotton控制模式開關]
• [小幫手協助攻擊，持續攻擊]

和'星星'圖樣的道具
• [無敵暴衝模式，移動速度變快、此時不受到敵人攻擊影響]

以及左上角'火山'圖樣的顯示
• [按下keyboard的S，可以清除所有敵人]


level 1完成後，可以進入level 2，此時前進一定程度後，另一種敵人出現，無法攻擊，只能閃避。

在level 2的後半段會出現boss，打完boss後，繼續前進，可以進入勝利畫面伴隨音效



# Basic Components Description : 
1. Jucify mechanisms : 
– Level : [level1較簡單，level2比較難]
– Skill : [遊玩一定程度之後可以根據指示放大絕，炸毀所有敵人]

2. Animations : [player會根據往左飛或往右飛，而呈現不同的飛行動畫]

3. Particle Systems : [player的bullet攻擊到enemy、boss，或是被敵人的bullet攻擊中時會有pixel噴發]

4. Sound effects : [有bgm、發射子彈的音效、和勝利畫面的音效]

5. UI : 
– Player health :  [右上角顯示player目前有幾條命]
– Score : [左上角顯示目前得分]
– Ultimate skill number or power : [前進一定程度後，左上角會出現圖示，按下keyboard的S，可以清除所有敵人]

6. Complete game process : 
– start menu => game view => level 1 game win => enter level 2 => level 2 win => back to menu

– if player life decrease to 0 => game over => back to menu

7. Basic rules:
• player : can move, shoot and it’s life will decrease when
touch enemy’s bullets or hit by advanced enemy
• Enemy : system should generate enemy and each enemy
can move and attack
• Map : background will move through the game


# Bonus Functions Description : 
1. Enhanced items : 玩家獲得'準心'圖樣的道具後
• [自動瞄準敵人的模式，透過ctrlBotton控制模式的開關]
• [攻擊變快速、變強，透過ctrlBotton控制模式開關]
• [小幫手協助攻擊，持續攻擊]
玩家獲得'星星'圖樣的道具後
• [無敵暴衝模式，移動速度變快、此時不受到敵人攻擊影響]

2. Boss : [自動追蹤玩家，並且同時多發子彈持續攻擊玩家，boss被攻擊15次才能打死]

3. Camera flash effect and shake effect : [玩家被擊中時產生特效]
