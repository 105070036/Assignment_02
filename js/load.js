var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);

        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 300, 'progressBar');
        progressBar.scale.setTo(0.6, 0.3);
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);

        // Load all game assets spriter
        game.load.spritesheet('player', 'assets/planeSpriter.png', 64.5, 54);
        game.load.image('enemy', 'assets/alientoy.png');
        game.load.image('ad_enemy', 'assets/alien.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.image('enemyBullet', 'assets/enemy-bullet.png');
        game.load.image('ad_enemyBullet', 'assets/alientoy.jpg');
        game.load.image('ultimate', 'assets/ultimateSkill.png');
        game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);

        game.load.image('bossbullet', 'assets/moon.png');
        game.load.image('boss', 'assets/snowman.png');    

        //  Little helper and his bullet
        game.load.image('littlehelper', 'assets/littlehelper.png');
        game.load.image('littlebullet', 'assets/littlebullet.png'); 

        //  Load the menu background
        game.load.image('background', 'assets/gamebackground.jpg');
        //  Load the scrolling starfield background
        game.load.image('starfield', 'assets/battlebackground.png');

        //  Load sound effects in game
        game.load.audio('bgmSound', 'assets/gamebgm.wav');
        game.load.audio('shootSound', 'assets/laserGun.wav');
        game.load.audio('winSound', 'assets/animalSting.wav');

        game.load.image('ultimateSkill', 'assets/ultimateSkill.png');
        game.load.image('specialCoin', 'assets/specialCoin.png');
        game.load.image('concentric', 'assets/concentric.png');
        game.load.image('helpericon', 'assets/helpericon.png');    


        game.load.image('wallH', 'assets/wallH.png');
        game.load.image('wallV', 'assets/wallV.png')

        // Load a new asset that we will use in the menu state
        
    },
    create: function() {
        // Go to the menu state
        game.state.start('menu');
    }
}; 