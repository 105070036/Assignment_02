var gameState = {
    preload: function() {
        /// Load block spritesheet.
        game.load.image('plane', 'assets/Plane.png');
    },
    create: function() {
        this.starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
    
        //  Player
        this.player = game.add.sprite(game.width/2, game.height/2+200, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.player, Phaser.Physics.ARCADE);;

        /// Player 4 animations.
        /// 1. Create the 'rightwalk' animation with frame rate = 8 by looping the frames 1 and 2
        this.player.animations.add('rightwalk', [4,5,6], 2, true);
        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [3,2,1], 2, true);

        //  Our bullet group
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);

        //  Little Helper and his bullet group
        this.littlehelper = game.add.image(game.width/2-100, game.height/2+200, 'littlehelper');
        this.littlehelper.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.littlehelper, Phaser.Physics.ARCADE);

        this.littlebullets = game.add.group();
        this.littlebullets.enableBody = true;
        this.littlebullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.littlebullets.createMultiple(30, 'littlebullet');
        this.littlebullets.setAll('anchor.x', 0.5);
        this.littlebullets.setAll('anchor.y', 1);
        this.littlebullets.setAll('outOfBoundsKill', true);
        this.littlebullets.setAll('checkWorldBounds', true);

        // Boss
        bosslife = 15;
        this.boss = game.add.group();
        this.boss.enableBody = true;
        this.boss.createMultiple(1, 'boss');
        this.boss.physicsBodyType = Phaser.Physics.ARCADE;

        //  Boss'bullets
        this.bossBullets = game.add.group();
        this.bossBullets.enableBody = true;
        this.bossBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bossBullets.createMultiple(10, 'bossbullet');
        this.bossBullets.setAll('anchor.x', 0.5);
        this.bossBullets.setAll('anchor.y', 0.5);
        this.bossBullets.setAll('outOfBoundsKill', true);
        this.bossBullets.setAll('checkWorldBounds', true);

        //Enemy
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;

        game.time.events.loop(200, this.addEnemy, this);

        // The enemy's bullets
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        // advanced Enemy
        this.ad_enemies = game.add.group();
        this.ad_enemies.enableBody = true;
        this.ad_enemies.createMultiple(4, 'ad_enemy');
        this.ad_enemies.physicsBodyType = Phaser.Physics.ARCADE;

        game.time.events.loop(400, this.add_adEnemy, this);


        //  Score
        this.scoreLabel = game.add.text(30, 30, 'score: 0', {font: '18px Arial', fill: '#ffffff'});
        game.global.score = 0;

        //  Lives
        lives = game.add.group();
        game.add.text(game.world.width - 100, 10, 'Lives', { font: '15px Arial', fill: '#fff' });

        for (var i = 0; i < 3; i++) {
            var plane = lives.create(game.world.width - 100 + (30 * i), 60, 'plane');
            plane.width = 50;
            plane.length = 30;
            plane.anchor.setTo(0.5, 0.5);
            plane.angle = 60;
            plane.alpha = 0.5;
        }

        //  Text
        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        //  Ultimate Skill Text and Image
        skillText = game.add.text(80,120, 'Press S kill them all', { font: '15px italic bold arial,serif', fill: '#fff' });
        skillText.anchor.setTo(0.5, 0.5);
        skillText.visible = false;

        ultimateSignal = 0;

        this.ultimateSkill = game.add.sprite(30, 55,  'ultimateSkill');
        skillText.anchor.setTo(0.5, 0.5);
        this.ultimateSkill.scale.setTo(0.4, 0.4);
        this.ultimateSkill.alpha = 0;

        //  Auto target shoot mode
        autoShootSignal = 0;
        autoShootMode = 1;

        //  Map scroll path
        scrollPath = 0;

        //  Start form mode 1
        mode = 1;


        //  Special coin for special moving mode
        var newPosition = game.rnd.pick(coinPosition);    
        this.coin = game.add.sprite(newPosition.x, newPosition.y, 'specialCoin');
        this.coin.anchor.setTo(0.5, 0.5);
        this.coin.scale.setTo(0.5, 0.5);
        this.coin.alpha = 0;
        game.physics.arcade.enable(this.coin);

        //  Auto target shooting icon
        var rndPosition = game.rnd.pick(iconPosition);    
        this.icon = game.add.sprite(rndPosition.x, rndPosition.y, 'concentric');
        this.icon.anchor.setTo(0.5, 0.5);
        this.icon.scale.setTo(0.2, 0.2);
        this.icon.alpha = 0;
        game.physics.arcade.enable(this.icon);

        // //  Little helper icon
        // var position = game.rnd.pick(helperPosition);    
        // this.helpericon = game.add.sprite(position.x, position.y, 'helpericon');
        // this.helpericon.anchor.setTo(0.5, 0.5);
        // this.helpericon.alpha = 0;
        // game.physics.arcade.enable(this.helpericon);


        //  Wall for invisible boundary
        this.walls = game.add.group();
        game.add.sprite(0, 0, 'wallH', 0, this.walls);
        game.add.sprite(0, 590, 'wallH', 0, this.walls);
        game.add.sprite(0, 0, 'wallV', 0, this.walls);
        game.add.sprite(790, 0, 'wallV', 0, this.walls);
        
        game.physics.arcade.enable(this.walls);
        this.walls.setAll('body.immovable', true);
        this.walls.alpha = 0;


        /// Particle
        this.emitter = game.add.emitter(0, 0, 45);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-250, 250);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;


        // game.physics.arcade.enable(this.player);
        // game.physics.arcade.enable(this.enemies);

        
        //  An explosion pool
        explosions = game.add.group();
        explosions.createMultiple(30, 'kaboom');
        explosions.forEach(this.setupInvader, this);

        //  Keyboard controls to play the game with
        this.cursor = game.input.keyboard.createCursorKeys();
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        ultimateButton = game.input.keyboard.addKey(Phaser.Keyboard.S);
    
        //  Auto target shoot to the enemy
        autoShootButton = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);

        //  BGM and Sound effect
        this.bgmSound = game.add.audio('bgmSound', 0.3);
        this.shootSound = game.add.audio('shootSound');
        this.winSound = game.add.audio('winSound');

        this.bgmSound.loop = true;
        this.bgmSound.play();
    
    },
    addBoss: function() {
        if(level == 1) {return;}
        // Get the first dead boss of the group
        var boss = this.boss.getFirstDead();
        // If there isn't any dead boss, do nothing
        if (!boss) {return;}
        /* Initialize boss */
        boss.width = 200;
        boss.height = 200;
        // Set the anchor point centered at the center
        boss.anchor.setTo(0.5, 0.5);
        // Put the boss above the top hole
        boss.reset(game.width/2, 100);
        boss.body.velocity.x = -100;
    },
    addEnemy:function(){
        // Get to the destination, no more enemy produced
        if(level == 1) {
            if(scrollPath >= 3600) {return;}
        }
        else if(level == 2){
            if(scrollPath >= 9600) {return;}
        }

        // Get the first dead enemy of the group
        var enemy = this.enemies.getFirstDead();
        // If there isn't any dead enemy, do nothing
        if (!enemy) {return;}
        /* Initialize enemy */
        enemy.width = 50;
        enemy.height = 50;
        // Set the anchor point centered at the center
        enemy.anchor.setTo(0.5, 0.5);
        // Put the enemy above the top hole
        enemy.reset(game.width/2-150 + 300 * game.rnd.integerInRange(-1, 1), 0);
        // Add gravity to see it fall
        enemy.body.gravity.y = 40;
        enemy.body.velocity.x = 50 + 30 * game.rnd.integerInRange(-1, 1);
        enemy.body.bounce.x = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    },
    add_adEnemy: function(){
        if(level == 1) {return;}

        // Get to the destination, no more enemy produced
        if(scrollPath <= 4000 || scrollPath >= 9600) {return;}


        //if(game.global.score < 200) {return;}

        // Get the first dead enemy of the group
        var adEnemy = this.ad_enemies.getFirstDead();
        // If there isn't any dead enemy, do nothing
        if (!adEnemy) {return;}
        /* Initialize enemy */
        adEnemy.width = 60;
        adEnemy.height = 60;
        // Set the anchor point centered at the center
        adEnemy.anchor.setTo(0.5, 0.5);
        // Put the enemy above the top hole
        adEnemy.reset(game.width/2-100 + 200*game.rnd.integerInRange(-1, 1), 0);
        // Add gravity to see it fall
        adEnemy.body.gravity.y = 50;
        adEnemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
        adEnemy.body.bounce.x = 1;
        adEnemy.checkWorldBounds = true;
        adEnemy.outOfBoundsKill = true;

    },
    setupInvader: function(invader) {

        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');
    
    },
    bulletHitsboss: function(bullet, boss) {
        if(boss == null) {return;}

        bullet.kill();
        if(bosslife == 0) {
            boss.kill();
            //  Increase the score
            game.global.score += 300;
            this.scoreLabel.text = 'score: ' + game.global.score;
        }
        else {
            bosslife -= 1;
        }

        //  Particl effect
        this.emitter.x = boss.x;
        this.emitter.y = boss.y;
        this.emitter.start(true, 800, null, 15);

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(boss.body.x, boss.body.y);
        explosion.play('kaboom', 100, false, true);
        
    },
    bulletHitsEnemy: function(bullet, enemy) {
        if(enemy == null) {return;}

        bullet.kill();
        enemy.kill();

        //  Particl effect
        this.emitter.x = enemy.x;
        this.emitter.y = enemy.y;
        this.emitter.start(true, 800, null, 15);

        //  Increase the score
        game.global.score += 20;
        this.scoreLabel.text = 'score: ' + game.global.score;

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 100, false, true);
        
    },
    littlebulletHitsEnemy: function(littlebullet, enemy) {
        if(enemy == null) {return;}
        
        littlebullet.kill();
        enemy.kill();

        //  Particl effect
        this.emitter.x = enemy.x;
        this.emitter.y = enemy.y;
        this.emitter.start(true, 800, null, 15);

        //  Increase the score
        game.global.score += 20;
        this.scoreLabel.text = 'score: ' + game.global.score;

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 100, false, true);
        
    },
    ad_enemyHitsPlayer: function(player, ad_enemy) {
        ad_enemy.kill();
        //  Particl effect
        this.emitter.x = ad_enemy.x;
        this.emitter.y = ad_enemy.y;
        this.emitter.start(true, 800, null, 15);

        live = lives.getFirstAlive();

        if (live)
        {
            live.kill();
        }
        
        //  Camera shake effect
        game.camera.shake(0.02, 300);

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 100, false, true);

        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            if(this.littlehelper) {this.littlehelper.kill()};

            this.enemyBullets.callAll('kill');
            this.littlebullets.callAll('kill');

            //  //  Camera flash effect
            game.camera.flash(0xffffff, 500);

            stateText.text=" GAME OVER \n Click to Menu";
            stateText.visible = true;

            this.enemies.removeAll();
            this.ad_enemies.removeAll();

            this.bgmSound.volume = 0;

            //the "click to restart" handler
            game.input.onTap.addOnce(this.restart,this);
        }

    },
    bossHitsPlayer: function(player, bossBullet){
        bossBullet.kill();

        live = lives.getFirstAlive();

        if (live)
        {
            live.kill();
        }

        //  Camera shake effect
        game.camera.shake(0.02, 300);

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 100, false, true);

        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            if(this.littlehelper) {this.littlehelper.kill()};

            this.enemyBullets.callAll('kill');
            this.littlebullets.callAll('kill');
            this.bossBullets.callAll('kill');

            //  //  Camera flash effect
            game.camera.flash(0xffffff, 500);

            stateText.text=" GAME OVER \n Click to Menu";
            stateText.visible = true;

            this.enemies.removeAll();
            this.ad_enemies.removeAll();
            this.boss.removeAll();

            this.bgmSound.volume = 0;

            //the "click to restart" handler
            game.input.onTap.addOnce(this.restart,this);
        }
    },
    enemyHitsPlayer: function(player, bullet) {
        bullet.kill();

        live = lives.getFirstAlive();

        if (live)
        {
            live.kill();
        }

        //  Camera shake effect
        game.camera.shake(0.02, 300);

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 100, false, true);

        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            if(this.littlehelper) {this.littlehelper.kill()};

            this.enemyBullets.callAll('kill');
            this.littlebullets.callAll('kill');
            
            //  //  Camera flash effect
            game.camera.flash(0xffffff, 500);

            stateText.text=" GAME OVER \n Click to Menu";
            stateText.visible = true;

            this.enemies.removeAll();
            this.ad_enemies.removeAll();

            this.bgmSound.volume = 0;

            //the "click to restart" handler
            game.input.onTap.addOnce(this.restart,this);
        }
        
    },
    playerHitsEnemy: function(player, enemy) {
        enemy.kill();

        //  Particl effect
        this.emitter.x = enemy.x;
        this.emitter.y = enemy.y;
        this.emitter.start(true, 800, null, 15);

        //  Increase the score
        game.global.score += 20;
        this.scoreLabel.text = 'score: ' + game.global.score;

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(enemy.body.x, enemy.body.y);
        explosion.play('kaboom', 100, false, true);
    },
    playerHitsAdenemy: function(player, ad_enemy) {
        ad_enemy.kill();

        //  Particl effect
        this.emitter.x = ad_enemy.x;
        this.emitter.y = ad_enemy.y;
        this.emitter.start(true, 800, null, 15);

        //  Increase the score
        game.global.score += 20;
        this.scoreLabel.text = 'score: ' + game.global.score;

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(ad_enemy.body.x, ad_enemy.body.y);
        explosion.play('kaboom', 100, false, true);
    },
    bossFires: function() {
        //  Grab the first bullet we can from the pool
        var bossBullet = this.bossBullets.getFirstExists(false);
    
        livingBoss.length=0;
    
        this.boss.forEachAlive(function(boss){
    
            // put every living enemy in an array
            livingBoss.push(boss);
        });
    
    
        if (bossBullet && livingBoss.length > 0)
        {
            
            var random = game.rnd.integerInRange(0,livingBoss.length-1);
    
            // randomly select one of them
            var shooter = livingBoss[random];
            // And fire the bullet from this enemy
            bossBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(bossBullet, this.player, 100);
            firingTimer = game.time.now + 1000;
        }
    },
    enemyFires: function () {

        //  Grab the first bullet we can from the pool
        var enemyBullet = this.enemyBullets.getFirstExists(false);
    
        livingEnemies.length=0;
    
        this.enemies.forEachAlive(function(enemy){
    
            // put every living enemy in an array
            livingEnemies.push(enemy);
        });
    
    
        if (enemyBullet && livingEnemies.length > 0)
        {
            
            var random = game.rnd.integerInRange(0,livingEnemies.length-1);
    
            // randomly select one of them
            var shooter = livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(enemyBullet, this.player,120);
            firingTimer = game.time.now + 1000;
        }
    
    },
    littlefireBullet: function () {

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > littelbulletTime)
        {
            //  Grab the first bullet we can from the pool
            var littlebullet = this.littlebullets.getFirstExists(false);

            if (littlebullet)
            {
                //  And fire it
                littlebullet.reset(this.littlehelper.x, this.littlehelper.y + 8);
                littelbulletTime = game.time.now + 150;
                littlebullet.body.velocity.y = -400;
            }
        }
    },
    fireBullet: function () {

        var target = this.enemies.getFirstAlive();
        // If there isn't any dead boss, do nothing
        if (!target) {return;}

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            var bullet = this.bullets.getFirstExists(false);

            if (bullet)
            {
                //  And fire it
                bullet.reset(this.player.x, this.player.y + 8);
                bulletTime = game.time.now + 200;

                if(autoShootSignal == 1 && autoShootMode == 1 && this.bgmSound.volume != 0 ){
                    this.enemies.forEachAlive(function(enemy){
                        if(50<=enemy.x && enemy.x <=750 && 50<=enemy.y && enemy.y<=550){
                            game.physics.arcade.moveToObject(bullet, enemy, 1600);
                            //console.log(enemy.body.x + ', ' + enemy.body.y);
                        }
                        else{
                            bullet.body.velocity.y = -1600;
                            bullet.scale.setTo(1.0, 5.0);
                        }
                    });
                }
                else {
                    bullet.body.velocity.y = -400;
                    bullet.scale.setTo(1.0, 1.0);
                }

                this.shootSound.play();
            }
        }
    },
    fireUltimateSkill: function() {
        if(ultimateSignal == 0) {return;}
        else ultimateSignal = 0; //can be only triggered once

        this.enemyBullets.callAll('kill');
        //this.winSound.play();

        //  Camera flash effect
        game.camera.flash(0xffffff, 500);

        this.enemies.forEach(function(enemy) {
            //  Create an explosion :)
            var explosion = explosions.getFirstExists(false);
            explosion.reset(enemy.body.x, enemy.body.y);
            explosion.play('kaboom', 100, false, true);
        }, this);

        this.enemies.callAll('kill');

        if(level == 2) {this.ad_enemies.callAll('kill');}
        
    },
    takeCoin: function (player, coin){
        if(this.coin.alpha == 0) {return;}
        else{
            mode = 2;
            coin.kill(); // Kill the coin.
        }
    },
    takeIcon: function (player, icon){
        if(this.icon.alpha == 0) {return;}
        else{
            autoShootSignal = 1;
            icon.kill(); // Kill the icon.
        }
    },
    resetBullet: function (bullet) {
        //  Called if the bullet goes out of the screen
        bullet.kill();
    },
    nextLevel: function() {
        //  And brings the aliens back from the dead :)
        this.enemies.removeAll();
        this.ad_enemies.removeAll();

        //resets the life count
        lives.callAll('revive');

        //revives the player
        this.player.revive();

        //hides the text
        stateText.visible = false;

        level = 2;

        this.create();
    },
    restart: function() {
        //  And brings the aliens back from the dead :)
        this.enemies.removeAll();
        this.ad_enemies.removeAll();

        //resets the life count
        lives.callAll('revive');

        //revives the player
        this.player.revive();
        
        //hides the text
        stateText.visible = false;

        // level start from level 1
        level = 1;

        this.playerDie();
    },
    update: function() {
        //  Scroll the background
        if(mode == 1) {
            this.starfield.tilePosition.y += 2;
            scrollPath += 2;
        }
        else if(mode == 2) {
            this.starfield.tilePosition.y += 8;
            scrollPath += 8;
        }
        

        if(scrollPath == 2000) {ultimateSignal = 1};
        if(scrollPath == 4500) {ultimateSignal = 1};

        if(ultimateSignal == 1){
            skillText.visible = true;
            this.ultimateSkill.alpha = 1;
        }
        else{
            skillText.visible = false;
            this.ultimateSkill.alpha = 0;
        }

        if(level == 1){
            if(scrollPath >= 3500) {ultimateSignal = 0};

            if(scrollPath >= 2000 && this.coin!=null) {this.coin.alpha = 1;}

            if(scrollPath >= 1000 && this.icon!=null) {this.icon.alpha = 1;}
        }
        else if(level == 2){

            if(scrollPath == 5000){
                this.addBoss();
            }

            if(scrollPath >= 9500) {ultimateSignal = 0;}

            if(scrollPath >= 6000 && this.coin!=null) {this.coin.alpha = 1;}

            if(scrollPath >= 2000 && this.icon!=null) {this.icon.alpha = 1;}
        }

        
        //console.log(scrollPath);

        if(lives.countLiving() >= 1){
            
            if(level == 1 && this.bgmSound.volume != 0){
                if(scrollPath >= 4200){
                    mode = 1;
                    this.enemyBullets.callAll('kill');
                    this.winSound.play();

                    //  Camera flash effect
                    game.camera.flash(0xffffff, 500);

                    stateText.text=" YOU FINISH! \n Click to Next level";
                    stateText.visible = true;

                    this.enemies.removeAll();
                    this.ad_enemies.removeAll();
                    
                    this.bgmSound.volume = 0;

                    //the "click to restart" handler
                    game.input.onTap.addOnce(this.nextLevel,this);
                    console.log(level);
                }
            }
            else if(level == 2 && this.bgmSound.volume != 0){
                if(scrollPath >= 10200){
                    mode = 1;
                    this.enemyBullets.callAll('kill');
                    this.winSound.play();

                    //  Camera flash effect
                    game.camera.flash(0xffffff, 500);

                    stateText.text=" YOU WIN! \n Click to Menu";
                    stateText.visible = true;

                    this.enemies.removeAll();
                    this.ad_enemies.removeAll();
                    this.boss.removeAll();

                    this.bgmSound.volume = 0;

                    //the "click to restart" handler
                    game.input.onTap.addOnce(this.restart,this);
                    console.log(level);
                }
            }
        }


        // Make the player and walls collide
        game.physics.arcade.collide(this.player, this.walls);

        if (!this.player.inWorld) { 
            this.restart();
        }

        if (this.player.alive)
        {
            //  Reset the player, then check for movement keys
            this.player.body.velocity.setTo(0, 0);
            
            if(mode == 1){
                this.movePlayer();
                //  Run collision
                game.physics.arcade.overlap(this.bullets, this.enemies, this.bulletHitsEnemy, null, this);
                game.physics.arcade.overlap(this.bullets, this.boss, this.bulletHitsboss, null, this);
                game.physics.arcade.overlap(this.enemyBullets, this.player, this.enemyHitsPlayer, null, this);
                game.physics.arcade.overlap(this.ad_enemies, this.player, this.ad_enemyHitsPlayer, null, this);
                game.physics.arcade.overlap(this.bossBullets, this.player, this.bossHitsPlayer, null, this);

                game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);
                game.physics.arcade.overlap(this.player, this.icon, this.takeIcon, null, this);
            }
            else if(mode == 2) {
                this.movePlayerMode2();
                game.physics.arcade.overlap(this.player, this.enemies, this.playerHitsEnemy, null, this);
                game.physics.arcade.overlap(this.player, this.ad_enemies, this.playerHitsAdenemy, null, this);
                game.physics.arcade.overlap(this.bullets, this.enemies, this.bulletHitsEnemy, null, this);
                game.physics.arcade.overlap(this.bullets, this.boss, this.bulletHitsboss, null, this);
                game.physics.arcade.overlap(this.player, this.icon, this.takeIcon, null, this);
                game.physics.arcade.overlap(this.bossBullets, this.player, this.bossHitsPlayer, null, this);
            }
            
            // Little helper
            if(autoShootSignal == 1){
                this.littlehelper.alpha = 1;
                this.littlefireBullet();
                game.physics.arcade.overlap(this.littlebullets, this.enemies, this.littlebulletHitsEnemy, null, this);
            }
            else{
                this.littlehelper.alpha = 0;
            }

            // little helper follows player
            this.littlehelper.x = this.player.x-50;
            this.littlehelper.y = this.player.y+40;

            this.boss.forEachAlive(function(boss){
                if(boss.body.x <=200){
                    boss.body.velocity.x = 100;
                    console.log('++');
                }
                else if(boss.x>=500){
                    boss.body.velocity.x = -100;
                    console.log('--');
                }

            });
           

            //  Firing?
            if (fireButton.isDown)
            {
                this.fireBullet();
            }

            if (game.time.now > firingTimer)
            {
                this.enemyFires();
            }

            if (game.time.now > firingTimer-300){
                this.bossFires();
            }

            //  Trigger Ultimate Skill
            if (ultimateButton.isDown){
                this.fireUltimateSkill();
            }

            //  Auto target shoot
            autoShootButton.onDown.add(this.autoShootModeChange, this);
        }

    }, 
    autoShootModeChange: function() {
        if(autoShootSignal == 0) {return;}

        if (autoShootMode == 0){
            autoShootMode = 1;
            console.log('autoShootMode is on, '+ autoShootMode );
        }
        else {
            autoShootMode = 0;
            console.log('autoShootMode is off, '+ autoShootMode );
        }
    },
    playerDie: function() {
        // When the player dies, go to the menu
        game.state.start('menu');
    },
    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('leftwalk');
            ///
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('rightwalk');
            ///
        }    
        else if (this.cursor.up.isDown) { 
            this.player.body.velocity.y = -200;
        }  
        else if (this.cursor.down.isDown) {
            this.player.body.velocity.y = 200;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;

            // Stop the animation
            this.player.animations.stop();
            this.player.frame = 0;
        }    
    },
    movePlayerMode2: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -400;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('leftwalk');
            ///
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 400;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('rightwalk');
            ///
        }    
        else if (this.cursor.up.isDown) { 
            this.player.body.velocity.y = -400;
        }  
        else if (this.cursor.down.isDown) {
            this.player.body.velocity.y = 400;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;

            // Stop the animation
            this.player.animations.stop();
            this.player.frame = 0;
        }    
    }
};

// Initialize Phaser
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');
// Define our global variable
game.global = { score: 0 };
// Add all the states
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('game', gameState);
// Start the 'boot' state
game.state.start('boot');

//let collideActivated = true;
var bulletTime = 0;
var littelbulletTime = 0;
var cursors;

var autoShootButton;
var autoShootSignal;
var autoShootMode;

var fireButton;
var ultimateButton;
var ultimateSignal = 0;
var explosions;
var enemyBullet;
var firingTimer = 0;
var lives;
var stateText;
var skillText;
var livingEnemies = [];
var livingBoss = [];

var level = 1;
var scrollPath = 0;

var mode = 1;

var bosslife = 15;

var coinPosition =
[{x: 140, y: 60}, {x: 360, y: 60}, // Top row
{x: 60, y: 140}, {x: 440, y: 140}, // Middle row
{x: 130, y: 300}, {x: 370, y: 300}]; // Bottom row

var iconPosition =
[{x: 190, y: 100}, {x: 310, y: 100}, // Top row
{x: 80, y: 200}, {x: 470, y: 200}, // Middle row
{x: 150, y: 420}, {x: 350, y: 420}]; // Bottom row

var helperPosition =
[{x: 240, y: 120}, {x: 270, y: 120}, // Top row
{x: 100, y: 250}, {x: 410, y: 250}, // Middle row
{x: 180, y: 370}, {x: 310, y: 370}]; // Bottom row