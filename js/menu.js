var menuState = {
    create: function() {
        // Add a background image
        game.add.image(0, 0, 'background');
        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 80, 'Raiden', { font: '50px italic bold arial,serif', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);

        // Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width/2, game.height/2, 'score: ' + game.global.score, { font: '25px italic bold arial,serif', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);

        // Explain how to start the game
        var startLabel = game.add.text(game.width/2, game.height-80, 'press Enter to start', { font: '25px italic bold arial,serif', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);

        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        // Call callback function when key is down.
        enterKey.onDown.add(this.start, this);
    },
    start: function() {
        // Start the actual game
        game.state.start('game');
    },
}; 
